import './App.css';
import Card from "./Card/Card";
import {Component} from "react";
import {CardDeck} from './CardDeck';
import {PokerHand} from './PokerHand.js';


class App extends Component {

    state = {
        randomCards : [],
        outcomes: ''
    };


    getRandomCards = () => {
        const cardDeck = new CardDeck();
        const cards = cardDeck.getCards(5);
        const pokerHand = new PokerHand(cards);
        pokerHand.isPair();
        const outcome = pokerHand.getOutCome();


        this.setState({
            randomCards: cards,
            outcomes : outcome
        })


    }
    render() {
        return (
            <div className='cardBlock'>
                <div>
                    <button onClick={this.getRandomCards} className='button'>Generate cards</button>
                </div>
                <div className="playingCards">
                    {this.state.randomCards.length && (
                        <div>
                            <Card rank={this.state.randomCards[0].rank} suit={this.state.randomCards[0].suit} icon={this.state.randomCards[0].icon}/>
                            <Card rank={this.state.randomCards[1].rank} suit={this.state.randomCards[1].suit} icon={this.state.randomCards[1].icon}/>
                            <Card rank={this.state.randomCards[2].rank} suit={this.state.randomCards[2].suit} icon={this.state.randomCards[2].icon}/>
                            <Card rank={this.state.randomCards[3].rank} suit={this.state.randomCards[3].suit} icon={this.state.randomCards[3].icon}/>
                            <Card rank={this.state.randomCards[4].rank} suit={this.state.randomCards[4].suit} icon={this.state.randomCards[4].icon}/>
                        </div>
                     )}
                </div>
                <p>{this.state.outcomes}</p>

            </div>
        );

    }
}

export default App;
