export const outcomes = {
    ONE_PAIR: 'one pair',
    TWO_PAIRS: 'two pair',
    THREE_PAIRS: 'three of a kind',
    FLUSH: 'flush',
    NOTHING: 'nothing'
}

export class PokerHand {
    constructor(cards) {
        this.cards = cards;
        this.suits = this.cards.map(card => card.suit);
        this.ranks = this.cards.map(card => card.rank);
        this.isFlush = this.suits.every(suit => suit === this.suits[0]);
    }

    isPair() {
        const ranksNumber = {};
        let pairIndex = 0;

        this.ranks.forEach(rank => {
            if (!ranksNumber[rank]) {
                ranksNumber[rank] = 1;
            } else {
                ranksNumber[rank]++;
            }
        });
        const rankValues = Object.values(ranksNumber);

        rankValues.forEach(value => {
            if (value === 2) {
                pairIndex++;
            }
        });

        if(pairIndex === 1) {
            return 'one pair';
        } else if(pairIndex === 2) {
            return 'two pair';
        } else if(rankValues.includes(3)) {
            return 'three of a kind';
        }
    }

    getOutCome() {
        if(this.isFlush) {
            return outcomes.FLUSH;
        } else if (this.isPair() === 'one pair') {
            return outcomes.ONE_PAIR;
        } else if(this.isPair() === 'two pair') {
            return outcomes.TWO_PAIRS;
        } else if(this.isPair() === 'three of a kind') {
            return outcomes.THREE_PAIRS;
        } else {
            return outcomes.NOTHING;
        }
    }
}
