export class CardDeck {
    constructor() {
        this.cards = [];
        this.suit = ['hearts', 'diams', 'clubs', 'spades'];
        this.rank = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
        this.suitIcons = ['♥', '♦️', '♣', '♠'];

        for (let i = 0; i < this.suit.length; i++) {
            for (let j = 0; j < this.rank.length; j++) {
                this.cards.push({suit : this.suit[i], rank : this.rank[j], icon: this.suitIcons[i]});
            }
        }
    }

    getCards(howMany) {
        const newArray = [];
        for (let i = 0; i < howMany; i++) {
            this.getCard();
            newArray.push(this.getCard());
        }
        return newArray;
    }

    getCard() {
        const randomCard = Math.floor(Math.random() * (this.cards.length - 1));
        const deleteCard = this.cards.splice(randomCard, 1);
        return deleteCard[0];
    }

}
