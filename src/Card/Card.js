import React from 'react';
import './cards.css';

const Card = props => {
    return (
        <div className={`card rank-${props.rank.toLowerCase()} ${props.suit}`}>
            <span className='rank'>{props.rank}</span>
            <span className='suit'>{props.icon}</span>
        </div>
    );
};

export default Card;